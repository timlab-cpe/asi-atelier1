Noms : 
- Labrosse Timothée
- Julia Rojkovska
- Laurine Dupont
- Nicolas Thouvenin

Cahier des charges :
- Elements réalisés
	- site web static avec la méthode GET de l'API qui implémente les données dans le HTML
	- formulaire et l’affichage d’une carte en Web Dynamique en simulant des données dans le programme
- Element non-réalisée
- Element réalisés en plus


Lien du site web static : https://gitlab.com/timlab-cpe/asi-atelier1/-/tree/static
Lien du site web dynamic : https://gitlab.com/timlab-cpe/asi-atelier1/-/tree/dynamic

Respect du MVC:  

Le MVC est réalisé avec répartition en package des modèles, des controlleurs et de la vue : 
- com.sp où se lance l'application principale
- com.sp.model où on stocke le modèle de la carte et le modèle du formulaire associé
- com.sp.controller qui contient cardDao & le requestCtr pour gérer les routes

Web statique : 
- Avantages : 
	- Possibilité d'y accèder en offline
	- Pas besoin de recharger toutes les pages
	- Vitesse de chargement des pages plus rapide
	- Plus sécurisé
- Inconvénients : 
	- Difficile à évoluer
	- Gestion moins efficace


Web dynamique : 
- Avantages : 
	- Intéressant quand il y a beaucoup de données ou des données amenées à changer fréquemment
	- Le Search Engine Optimization est simple
- Inconvénients : 
	- Performance
	- Division du back et du front plus difficile
	- Impossible d'y accèder en offline 
